package csv2pg

import (
	"bytes"
	"context"
	"database/sql"
	"fmt"
	"strconv"
	"time"

	"github.com/google/uuid"
)

// ProcessCSVRow processes each csvRow directly from csv.Reader.Read().
//
// This function is exported to support looping by CSV rows handled outside
// the provided ProcessCSV function (useful, ie. processing your own action per row)
func (instance *CSV2PG) ProcessCSVRow(csvRow []string) (err error) {
	if !instance.initialized {
		return fmt.Errorf("CSV2PG instance has not been initialized")
	}
	csvRowMap := make(map[string]string)
	returnsMap := make(map[string]map[string]interface{})
	for i, v := range csvRow {
		csvRowMap[instance.csvHeaders[i]] = v
	}
	aux := instance.getAuxiliaries()
	if aux.TransactionLevel == TransactionLevelRow {
		aux.tx, err = aux.db.BeginTx(context.Background(), &sql.TxOptions{Isolation: sql.LevelDefault, ReadOnly: false})
		if err != nil {
			aux.log(LogDebug, "Failed to begin CSVrow transaction")
			return err
		}
	}
	for _, insertDef := range instance.Inserts {
		err = insertDef.processCSVRow(aux, csvRowMap, returnsMap)
		if err != nil {
			if instance.TransactionLevel == TransactionLevelRow {
				aux.log(LogDebug, fmt.Sprintf("Rolling back %s transaction", instance.TransactionLevel))
				aux.tx.Rollback()
			}
			aux.log(LogDebug, fmt.Sprintf("Table %s insertion FAILED for CSV row %v", insertDef.TableName, csvRow))
			return err
		}
		aux.log(LogDebug, fmt.Sprintf("Table %s insertion SUCCESS for CSV row %v", insertDef.TableName, csvRow))
	}
	if aux.TransactionLevel == TransactionLevelRow {
		aux.log(LogDebug, "Committing CSVrow transaction")
		err = aux.tx.Commit()
		if err != nil {
			return err
		}
	}
	return nil
}
func (def *InsertDef) processCSVRow(aux aux, csvRow map[string]string, returnsMap map[string]map[string]interface{}) (err error) {
	if def.haveIdentifiers {
		existBefore, err := def.doPreSelect(aux, csvRow, returnsMap)
		if err != nil {
			return err
		}
		if existBefore {
			if !def.ShouldUpdate {
				return fmt.Errorf("table %s insertion has pre-existing row", def.TableName)
			}
			if def.haveUpdatables {
				aux.log(LogDebug, fmt.Sprintf("CSV row data for table %s exists in DB, performing update", def.TableName))
				return def.doUpdate(aux, csvRow, returnsMap)
			}
			aux.log(LogDebug, fmt.Sprintf("CSV row data for table %s (no updatables) exists in DB, skipping update", def.TableName))
			return nil
		} else {
			aux.log(LogDebug, fmt.Sprintf("CSV row data for table %s is new, performing insert", def.TableName))
			return def.doInsert(aux, csvRow, returnsMap)
		}
	}
	aux.log(LogDebug, fmt.Sprintf("CSV row data for table %s (no identifiers), performing insert", def.TableName))
	return def.doInsert(aux, csvRow, returnsMap)
}

func (def *InsertDef) doPreSelect(aux aux, csvRow map[string]string, returnsMap map[string]map[string]interface{}) (bool, error) {
	bindings := make([]interface{}, def.selectBindsLength)
	colIdx := -1
	for _, colDef := range def.Columns {
		err := colDef.processColValue(processSelectColValue, bindings, &colIdx, csvRow, returnsMap)
		if err != nil {
			return false, err
		}
	}
	aux.log(LogDebug, fmt.Sprintf("Performing preselect statement: %s\n    with bindings %v\n", def.selectStatement, bindings))
	var resultRows *sql.Rows
	var err error
	if aux.tx != nil {
		resultRows, err = aux.tx.Query(def.selectStatement, bindings...)
	} else {
		resultRows, err = aux.db.Query(def.selectStatement, bindings...)
	}
	if err != nil {
		return false, err
	}
	defer resultRows.Close()
	preExist := resultRows.Next()
	if preExist {
		resultMap := make(map[string]interface{})
		columnTypes, _ := resultRows.ColumnTypes()
		values := make([]interface{}, len(columnTypes))
		for idx := range values {
			values[idx] = new(interface{})
		}
		err := resultRows.Scan(values...)
		if err == nil {
			for idx, value := range values {
				resultMap[columnTypes[idx].Name()] = *(value.(*interface{}))
			}
		}
	}
	return preExist, err
}

func (def *InsertDef) doInsert(aux aux, csvRow map[string]string, returnsMap map[string]map[string]interface{}) error {
	bindings := make([]interface{}, def.insertBindsLength)
	colIdx := -1
	for _, colDef := range def.Columns {
		err := colDef.processColValue(processInsertColValue, bindings, &colIdx, csvRow, returnsMap)
		if err != nil {
			return err
		}
	}
	aux.log(LogDebug, fmt.Sprintf("Performing insert statement: %s\n    with bindings %v\n", def.insertStatement, bindings))
	var resultRows *sql.Rows
	var err error
	if aux.tx != nil {
		resultRows, err = aux.tx.Query(def.insertStatement, bindings...)
	} else {
		resultRows, err = aux.db.Query(def.insertStatement, bindings...)
	}
	if err != nil {
		return err
	}
	defer resultRows.Close()
	if len(def.Returns) > 0 {
		resultMap := make(map[string]interface{})
		hasNext := resultRows.Next()
		if hasNext {
			columnTypes, _ := resultRows.ColumnTypes()
			values := make([]interface{}, len(columnTypes))
			for idx := range values {
				values[idx] = new(interface{})
			}
			err := resultRows.Scan(values...)
			if err != nil {
				return err
			}
			for idx, value := range values {
				resultMap[columnTypes[idx].Name()] = *(value.(*interface{}))
			}
		}
		returnsMap[def.Name] = resultMap
	}
	return nil
}

func (def *InsertDef) doUpdate(aux aux, csvRow map[string]string, returnsMap map[string]map[string]interface{}) error {
	bindings := make([]interface{}, def.updateBindsLength)
	colIdx := -1
	for _, colDef := range def.Columns {
		err := colDef.processColValue(processUpdateColValue, bindings, &colIdx, csvRow, returnsMap)
		if err != nil {
			return err
		}
	}
	for _, colDef := range def.Columns {
		err := colDef.processColValue(processUpdateWhereColValue, bindings, &colIdx, csvRow, returnsMap)
		if err != nil {
			return err
		}
	}
	aux.log(LogDebug, fmt.Sprintf("Performing update statement: %s\n    with bindings %v\n", def.updateStatement, bindings))
	var resultRows *sql.Rows
	var err error
	if aux.tx != nil {
		resultRows, err = aux.tx.Query(def.updateStatement, bindings...)
	} else {
		resultRows, err = aux.db.Query(def.updateStatement, bindings...)
	}
	if err != nil {
		return err
	}
	defer resultRows.Close()
	if len(def.Returns) > 0 {
		resultMap := make(map[string]interface{})
		hasNext := resultRows.Next()
		if hasNext {
			columnTypes, _ := resultRows.ColumnTypes()
			values := make([]interface{}, len(columnTypes))
			for idx := range values {
				values[idx] = new(interface{})
			}
			err := resultRows.Scan(values...)
			if err != nil {
				return err
			}
			for idx, value := range values {
				resultMap[columnTypes[idx].Name()] = *(value.(*interface{}))
			}
		}
		returnsMap[def.Name] = resultMap
	}
	return nil
}

const (
	processSelectColValue = iota
	processInsertColValue
	processUpdateColValue
	processUpdateWhereColValue
)

func (colDef *ColDef) processColValue(processFor int, bindings []interface{}, colIdx *int, csvRow map[string]string, returnsMap map[string]map[string]interface{}) (err error) {
	switch processFor {
	case processSelectColValue:
		{
			if !colDef.IsIdentifier {
				return nil
			}
		}
	case processUpdateWhereColValue:
		{
			if !colDef.IsIdentifier {
				return nil
			}
		}
	case processUpdateColValue:
		{
			if !colDef.ShouldUpdate {
				return nil
			}
		}
	}
	var sourceValue string
	switch colDef.colSource {
	case colSourceGeneratorSQL:
		return nil
	case colSourceReturns:
		{
			nextIdx := *colIdx + 1
			*colIdx = nextIdx
			bindings[*colIdx] = returnsMap[colDef.FromPath[0]][colDef.FromPath[1]]
			return nil
		}
	case colSourceCsv:
		{
			nextIdx := *colIdx + 1
			*colIdx = nextIdx
			sourceValue = csvRow[colDef.CSVColName]
		}
	case colSourceDynamicCsv:
		{
			nextIdx := *colIdx + 1
			*colIdx = nextIdx
			dynamicTextBuf := bytes.NewBufferString("")
			err := colDef.dynamicTextTemplate.Execute(dynamicTextBuf, csvRow)
			if err != nil {
				return err
			}
			sourceValue = dynamicTextBuf.String()
		}
	default:
		{
			return fmt.Errorf("invalid column data source type for column %s", colDef.SQLColName)
		}
	}
	if colDef.validationRegexp != nil {
		if !colDef.validationRegexp.MatchString(sourceValue) {
			return fmt.Errorf("column %s source value %s failed Validation Regex %s", colDef.SQLColName, sourceValue, colDef.ValidationRegex)
		}
	}
	switch colDef.SQLColType {
	case BoolCol:
		{
			bindings[*colIdx] = colDef.booleanRegexp.MatchString(sourceValue)
		}
	case StringCol:
		{
			bindings[*colIdx] = sourceValue
		}
	case TimestampTzCol:
	case TimestampCol:
	case DateCol:
		{
			if timeValue, err := time.Parse(colDef.TimestampFormat, sourceValue); err == nil {
				bindings[*colIdx] = timeValue
			} else {
				return err
			}
		}
	case FloatCol:
		{
			parsedValue, err := strconv.ParseFloat(sourceValue, 64)
			if err != nil {
				return err
			}
			bindings[*colIdx] = parsedValue
		}
	case IntCol:
		{
			parsedValue, err := strconv.ParseInt(sourceValue, 10, 64)
			if err != nil {
				return err
			}
			bindings[*colIdx] = parsedValue
		}
	case UUIDCol:
		{
			parsedValue, err := uuid.Parse(sourceValue)
			if err != nil {
				return err
			}
			bindings[*colIdx] = parsedValue
		}
	}
	return nil
}
