module gitlab.com/benedictjohannes/csv2pg

go 1.16

require (
	github.com/eknkc/basex v1.0.0
	github.com/google/uuid v1.2.0
	// for CLI
	github.com/lib/pq v1.10.0
	gopkg.in/yaml.v2 v2.4.0
)
