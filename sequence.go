package csv2pg

import (
	"context"
	"database/sql"
	"encoding/csv"
	"fmt"
	"io"
)

// ProcessCSV performs the whole sequence of reading CSV headers, initializing instance
// and performing reading the CSV into database
func (instance *CSV2PG) ProcessCSV(db *sql.DB, csvFile io.Reader) (successCount, failCount int, err error) {
	csvReader := csv.NewReader(csvFile)
	if len(instance.CSVCommaChar) == 1 {
		csvReader.Comma = rune(byte(instance.CSVCommaChar[0]))
	}
	if len(instance.CSVCommentChar) == 1 {
		csvReader.Comment = rune(byte(instance.CSVCommentChar[0]))
	}
	if instance.CSVLazyQuotes {
		csvReader.LazyQuotes = true
	}
	if instance.CSVTrimLeadingSpace {
		csvReader.TrimLeadingSpace = true
	}
	if instance.CSVTrailingComma {
		csvReader.TrailingComma = true
	}
	instance.connectLogger()
	instance.log(LogDebug, "Starting to read CSV")
	csvHeaderArr, err := csvReader.Read()
	if err != nil {
		if err == io.EOF {
			instance.log(LogSummary, "Empty CSV supplied")
			return successCount, failCount, fmt.Errorf("empty CSV supplied")
		}
		instance.log(LogSummary, fmt.Sprintf("Error reading CSV: %s", err))
		return successCount, failCount, fmt.Errorf("error reading CSV: %s", err)

	}
	instance.log(LogDebug, fmt.Sprintf("CSV header OK: %v", csvHeaderArr))
	err = instance.InitializeAndValidate(db, csvHeaderArr)
	if err != nil {
		instance.log(LogSummary, fmt.Sprintf("Error in initializing: %s", err))
		return successCount, failCount, err
	}
	aux := instance.getAuxiliaries()
	if aux.TransactionLevel == TransactionLevelGlobal {
		instance.log(LogDebug, "Beginning global transaction")
		instance.tx, err = aux.db.BeginTx(context.Background(), &sql.TxOptions{Isolation: sql.LevelDefault, ReadOnly: false})
		aux = instance.getAuxiliaries()
		if err != nil {
			instance.log(LogSummary, fmt.Sprintf("Error in beginning global transaction: %s", err))
			return successCount, failCount, err
		}
	}
	var rowCounter int
	for {
		csvRow, err := csvReader.Read()
		if err == io.EOF {
			instance.log(LogInfo, fmt.Sprintf("Reached end of CSV file on (data)line %d", rowCounter))
			break
		}
		rowCounter++
		if err != nil {
			break
		}
		aux.log(LogDebug, fmt.Sprintf("Processing CSV row on (data)line %d: %v", rowCounter, csvRow))
		err = instance.ProcessCSVRow(csvRow)
		if err != nil {
			failCount++
			aux.log(LogFailedTransaction, fmt.Sprintf("FAILED[%d]: %v", rowCounter, csvRow))
			if instance.StopOnCSVRowError {
				if instance.TransactionLevel == TransactionLevelGlobal {
					aux.log(LogDebug, fmt.Sprintf("Rolling back %s transaction", instance.TransactionLevel))
					aux.tx.Rollback()
				}
				aux.log(LogSummary, fmt.Sprintf("Stopping due to error processing CSV row with error: %s\n    with data %v", err, csvRow))
				return successCount, failCount, err
			}
		} else {
			aux.log(LogAllTransaction, fmt.Sprintf("SUCCESS[%d]: %v", rowCounter, csvRow))
			successCount++
		}
	}
	if aux.TransactionLevel == TransactionLevelGlobal {
		instance.log(LogDebug, "Committing global transaction")
		err := aux.tx.Commit()
		if err != nil {
			instance.log(LogSummary, "Error committing global transaction")
		}
		return successCount, failCount, err
	}
	aux.log(LogSummary, fmt.Sprintf("CSV processed to DB with %d successes and %d fails\n", successCount, failCount))
	return successCount, failCount, err
}
