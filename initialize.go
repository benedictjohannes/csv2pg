package csv2pg

import (
	"bytes"
	"database/sql"
	"errors"
	"fmt"
	"regexp"
	"strings"
	"text/template"
	"time"
)

// InitializeAndValidate must be called before a CSV2PG instance is used to process CSV.
// The instance to be initiated should already has its struct fields assigned when non default.
// An instance *must* have its `InsertDef`s configured to tables the CSV shall be processed into.
//
// This will (1) connect supplied *sql.DB connection,
// (2) validate each InsertDef and its columns for valid configuration
// against CSV column header and SQL tables by querying INFORMATION_SCHEMA,
// (3) initiate logger (defaults: LogOutput to os.Stdout, LogLevel to summary), and
// (4) merge default FuncMaps into supplied FuncMaps (if supplied)
func (instance *CSV2PG) InitializeAndValidate(db *sql.DB, csvHeader []string) error {
	instance.connectLogger()
	instance.csvHeaders = csvHeader
	instance.db = db
	var returnsList = make(map[string][]string)
	if len(csvHeader) == 0 {
		return errors.New("zero length CSV header")
	}
	var csvHeaderMap = make(map[string]bool)
	for _, csvCol := range csvHeader {
		csvHeaderMap[csvCol] = true
	}
	if instance.FuncMaps == nil {
		instance.FuncMaps = make(template.FuncMap)
	}
	for key, value := range stdFuncMap {
		if _, ok := instance.FuncMaps[key]; !ok {
			instance.FuncMaps[key] = value
		}
	}
	if instance.TransactionLevel != TransactionLevelRow && instance.TransactionLevel != TransactionLevelGlobal {
		instance.TransactionLevel = TransactionLevelNone
	}
	aux := instance.getAuxiliaries()
	aux.log(LogDebug, "Initializing table inserters (InsertDefs)")
	if len(instance.Inserts) < 1 {
		aux.log(LogSummary, "CSV2PG not configured with InsertDefs")
		return errors.New("zero length InsertDefs")
	}
	for _, insertDef := range instance.Inserts {
		if insertDef.Name != "" && len(insertDef.Returns) > 0 {
			returnsList[insertDef.Name] = insertDef.Returns
		}
		err := insertDef.initializeAndValidate(aux, csvHeaderMap, returnsList)
		if err != nil {
			aux.log(LogSummary, fmt.Sprintf("Table inserter (InsertDef) initialization: Table %s failed: %s", insertDef.TableName, err))
			return err
		}
		aux.log(LogDebug, fmt.Sprintf("Table %s initialized", insertDef.TableName))
	}
	aux.log(LogDebug, "Table inserters (InsertDefs) initialized")
	instance.initialized = true
	instance.log(LogInfo, "Inserter intialization: OK")
	return nil
}

const colSourceCsv = "CSV Column"
const colSourceDynamicCsv = "CSV Dynamic Text"
const colSourceReturns = "SQL returns FromPath"
const colSourceGeneratorSQL = "SQL Expression"

var validSQLColNameRegex = regexp.MustCompile("^[a-zA-Z_][a-zA-Z0-9_]*$")

func (def *InsertDef) initializeAndValidate(aux aux, headerMap map[string]bool, returnsList map[string][]string) (err error) {
	err = def.getSQLColsInfo(aux.db)
	if err != nil {
		return nil
	}
	err = def.initializeCols()
	if err != nil {
		return nil
	}
	err = def.initializeReturns()
	if err != nil {
		return err
	}
	insertStatement := fmt.Sprintf("insert into \"%s\" (", def.TableName)
	var insertHasStarted bool
	var insertBindCounter = 0
	var insertBindStatementPart string
	selectStatement := fmt.Sprintf("select * from \"%s\" where ", def.TableName)
	var selectBindCounter = 0
	updateStatement := fmt.Sprintf("update \"%s\" ", def.TableName)
	var updateHasStarted bool
	var updateBindCounter = 0
	var updateWhereClauseHasStarted bool
	for _, colDef := range def.Columns {
		multipleSourceErr := "table %s column %s source %s has been defined but %s added"
		if colDef.CSVColName != "" {
			if !headerMap[colDef.CSVColName] {
				return fmt.Errorf("CSV file does not have CSV column %s specified in config", colDef.CSVColName)
			} else {
				colDef.colSource = colSourceCsv
			}
		}
		if colDef.CSVDynamicText != "" {
			if colDef.colSource != "" {
				return fmt.Errorf(multipleSourceErr, def.TableName, colDef.SQLColName, colDef.colSource, colSourceCsv)
			}
			colDef.dynamicTextTemplate, err = template.New(fmt.Sprint(def.Name, colDef.SQLColName)).Funcs(aux.FuncMaps).Parse(colDef.CSVDynamicText)
			if err != nil {
				return err
			}
			csvColMap := make(map[string]string)
			for i, csvCol := range colDef.CSVDynamicTextCols {
				if !headerMap[csvCol] {
					return fmt.Errorf("CSV does not contain CSVToTextCol \"%s\"", csvCol)
				}
				csvColMap[csvCol] = fmt.Sprintf("col%d_%s", i, csvCol)
			}
			testTemplateWriteDestination := bytes.NewBufferString("")
			err = colDef.dynamicTextTemplate.Execute(testTemplateWriteDestination, csvColMap)
			if err != nil {
				return fmt.Errorf("CSVColsToText failed to execute \"%s\" with %v", colDef.CSVDynamicText, csvColMap)
			}
			colDef.colSource = colSourceDynamicCsv
		}
		if len(colDef.FromPath) > 0 {
			if colDef.colSource != "" {
				return fmt.Errorf(multipleSourceErr, def.TableName, colDef.SQLColName, colDef.colSource, colSourceDynamicCsv)
			}
			if len(colDef.FromPath) != 2 {
				return fmt.Errorf("table %s column %s FromPath length != 2", def.TableName, colDef.SQLColName)
			}
			if returnCols, ok := returnsList[colDef.FromPath[0]]; ok {
				var hasReturnCol bool
				for returnColI, returnCol := range returnCols {
					if (returnColI == 0 && returnCol == "*") || returnCol == colDef.FromPath[1] {
						hasReturnCol = true
						break
					}
				}
				if !hasReturnCol {
					return fmt.Errorf("table %s column %s FromPath[%s,%s] does not exists", def.TableName, colDef.SQLColName, colDef.FromPath[0], colDef.FromPath[1])
				}
			}
			colDef.colSource = colSourceReturns
		}
		if colDef.GeneratorSQL != "" {
			if colDef.colSource != "" {
				return fmt.Errorf(multipleSourceErr, def.TableName, colDef.SQLColName, colDef.colSource, colSourceDynamicCsv)
			}
			if strings.Contains(colDef.GeneratorSQL, ";") {
				return fmt.Errorf("table %s column %s GeneratorSQL %s contains venerable SQL injection charactor \";\"", def.TableName, colDef.SQLColName, colDef.GeneratorSQL)
			}
			if colDef.IsIdentifier {
				return fmt.Errorf("table %s column %s IsIdentifier = true while source is GeneratorSQL", def.TableName, colDef.SQLColName)
			}
			row := aux.db.QueryRow(fmt.Sprintf("select %s as s;", colDef.GeneratorSQL))
			if row.Err() != nil {
				return fmt.Errorf("table %s column %s GeneratorSQL %s failed to execute", def.TableName, colDef.SQLColName, colDef.GeneratorSQL)
			}
			colDef.colSource = colSourceGeneratorSQL
		}
		if colDef.colSource == "" {
			return fmt.Errorf("unable to determine column source for Table %s Column %s", def.TableName, colDef.SQLColName)
		}
		if !insertHasStarted {
			insertHasStarted = true
			insertStatement += fmt.Sprintf("\"%s\"", colDef.SQLColName)
			if colDef.colSource == colSourceGeneratorSQL {
				insertBindStatementPart += colDef.GeneratorSQL
			} else {
				insertBindCounter++
				insertBindStatementPart += fmt.Sprintf("$%d", insertBindCounter)
				colDef.bindArrIdx = insertBindCounter
			}
		} else {
			insertStatement += fmt.Sprintf(", \"%s\"", colDef.SQLColName)
			if colDef.colSource == colSourceGeneratorSQL {
				insertBindStatementPart += fmt.Sprintf(", %s", colDef.GeneratorSQL)
			} else {
				insertBindCounter++
				insertBindStatementPart += fmt.Sprintf(", $%d", insertBindCounter)
				colDef.bindArrIdx = insertBindCounter
			}
		}
		if colDef.IsIdentifier {
			if selectBindCounter == 0 {
				selectBindCounter++
				selectStatement += fmt.Sprintf("\"%s\" = $%d", colDef.SQLColName, selectBindCounter)
			} else {
				selectBindCounter++
				selectStatement += fmt.Sprintf(" and \"%s\" = $%d", colDef.SQLColName, selectBindCounter)
			}
		}
		if colDef.ShouldUpdate {
			if !updateHasStarted {
				updateHasStarted = true
				if colDef.colSource == colSourceGeneratorSQL {
					updateStatement += fmt.Sprintf("set %s = %s", colDef.SQLColName, colDef.GeneratorSQL)
				} else {
					updateBindCounter++
					updateStatement += fmt.Sprintf("set \"%s\" = $%d", colDef.SQLColName, updateBindCounter)
				}
			} else {
				if colDef.colSource == colSourceGeneratorSQL {
					updateStatement += fmt.Sprintf(", %s = %s", colDef.SQLColName, colDef.GeneratorSQL)
				} else {
					updateBindCounter++
					updateStatement += fmt.Sprintf(", \"%s\" = $%d", colDef.SQLColName, updateBindCounter)
				}
			}
		}
	}
	if selectBindCounter > 0 {
		def.haveIdentifiers = true
		def.selectBindsLength = selectBindCounter
		def.selectStatement = selectStatement + ";"
		if updateBindCounter > 0 {
			for _, colDef := range def.Columns {
				if colDef.IsIdentifier {
					if !updateWhereClauseHasStarted {
						updateWhereClauseHasStarted = true
						updateBindCounter++
						updateStatement += fmt.Sprintf(" where \"%s\" = $%d", colDef.SQLColName, updateBindCounter)
					} else {
						updateBindCounter++
						updateStatement += fmt.Sprintf(" and \"%s\" = $%d", colDef.SQLColName, updateBindCounter)
					}
				}
			}
			def.haveUpdatables = true
			def.updateBindsLength = updateBindCounter
			def.updateStatement = updateStatement + def.returningStatementPart + ";"
		}
	}
	insertStatement += fmt.Sprintf(") values (%s) %s;", insertBindStatementPart, def.returningStatementPart)
	def.insertStatement = insertStatement
	def.insertBindsLength = insertBindCounter
	return nil
}

func (def *InsertDef) getSQLColsInfo(db *sql.DB) error {
	var dbColsInfo []dbColDef
	rows, err := db.Query("select column_name, udt_name from information_schema.columns where table_name = $1", def.TableName)
	if err != nil {
		return err
	}
	defer rows.Close()
	for rows.Next() {
		var currentRow = dbColDef{}
		err = rows.Scan(&currentRow.colName, &currentRow.dataType)
		if err != nil {
			return err
		}
		dbColsInfo = append(dbColsInfo, currentRow)
		if len(def.Returns) == 1 && def.Returns[0] == "*" {
			def.returnAllArr = append(def.returnAllArr, currentRow.colName)
		}
	}
	def.dbColsInfo = dbColsInfo
	return nil
}

func (def *InsertDef) initializeCols() (err error) {
	var sqlCols = make(map[string]bool)
	for colIdx, colDef := range def.Columns {
		if !validSQLColNameRegex.MatchString(colDef.SQLColName) {
			return fmt.Errorf("table %s column[%d] %s contains illegal SQL column name", def.TableName, colIdx, colDef.SQLColName)
		}
		if sqlCols[colDef.SQLColName] {
			return fmt.Errorf("table %s column[%d] %s inserted to same column multiple times", def.TableName, colIdx, colDef.SQLColName)
		}
		if colDef.ShouldUpdate && colDef.IsIdentifier {
			return fmt.Errorf("table %s column[%d] %s IsIdentifer and ShouldUpdate are both true", def.TableName, colIdx, colDef.SQLColName)
		}
		if colDef.ValidationRegex != "" {
			colDef.validationRegexp, err = regexp.Compile(colDef.ValidationRegex)
			if err != nil {
				return fmt.Errorf("table %s with column %s ValidationRegex %s failed to compile", def.TableName, colDef.SQLColName, colDef.ValidationRegex)
			}
		}
		sqlCols[colDef.SQLColName] = true
		var hasSQLCol bool
		if (len(colDef.CSVColName) != 0 || len(colDef.CSVDynamicText) != 0) && (colDef.SQLColType == TimestampCol || colDef.SQLColType == TimestampTzCol) {
			if _, err = time.Parse(colDef.TimestampFormat, colDef.TimestampFormat); len(colDef.TimestampFormat) == 0 || err != nil {
				return fmt.Errorf("table %s Columns %s TimestampFormat %s is not valid for timestamp column", def.TableName, colDef.SQLColName, colDef.TimestampFormat)
			}
		}
		for _, sqlInfoCol := range def.dbColsInfo {
			if sqlInfoCol.colName == colDef.SQLColName {
				hasSQLCol = true
				const typeMismatchErrTemplate = "table %s column %s type %s not matched with %s in database"
				typeMismatchErr := fmt.Errorf(typeMismatchErrTemplate, def.TableName, colDef.SQLColName, colDef.SQLColType, sqlInfoCol.dataType)
				switch colDef.SQLColType {
				case BoolCol:
					if sqlInfoCol.dataType == "bool" {
						colDef.booleanRegexp, err = regexp.Compile(colDef.BooleanRegex)
						if err != nil {
							return fmt.Errorf("table %s with column %s BooleanRegex %s failed to compile", def.TableName, colDef.SQLColName, colDef.BooleanRegex)
						}
						break
					} else {
						return typeMismatchErr
					}
				case StringCol:
					if sqlInfoCol.dataType == "text" || strings.Contains(sqlInfoCol.dataType, "varchar") {
						break
					} else {
						return typeMismatchErr
					}
				case TimestampTzCol:
					if sqlInfoCol.dataType == "timestamptz" {
						break
					} else {
						return typeMismatchErr
					}
				case TimestampCol:
					if sqlInfoCol.dataType == "timestamp" {
						break
					} else {
						return typeMismatchErr
					}
				case DateCol:
					if strings.Contains(sqlInfoCol.dataType, "date") {
						break
					} else {
						return typeMismatchErr
					}
				case FloatCol:
					if strings.Contains(sqlInfoCol.dataType, "float") {
						break
					} else {
						return typeMismatchErr
					}
				case IntCol:
					if strings.Contains(sqlInfoCol.dataType, "int") {
						break
					} else {
						return typeMismatchErr
					}
				case UUIDCol:
					if sqlInfoCol.dataType == "uuid" {
						break
					} else {
						return typeMismatchErr
					}
				default:
					{
						return fmt.Errorf("table %s column %s has invalid SQL column type \"%s\" defined", def.TableName, colDef.SQLColName, colDef.SQLColType)
					}
				}
			}
		}
		if !hasSQLCol {
			return fmt.Errorf("table %s column %s does not exist in database", def.TableName, colDef.SQLColName)
		}
	}
	return nil
}

func (def *InsertDef) initializeReturns() error {
	if len(def.Returns) == 0 {
		def.returningStatementPart = ""
		return nil
	}
	returningStatement := " returning "
	for returnIdx, returnCol := range def.Returns {
		if strings.Contains(returnCol, ";") {
			return fmt.Errorf("table %s Returns[%d] %s contains venerable SQL injection charactor \";\"", def.TableName, returnIdx, returnCol)
		}
		if returnIdx == 0 && returnCol == "*" {
			for returnAllIdx, returnAllCol := range def.returnAllArr {
				if returnAllIdx == 0 {
					returningStatement += returnAllCol
				} else {
					returningStatement += fmt.Sprintf(", %s", returnAllCol)
				}
			}
			break
		}
		if returnIdx == 0 {
			returningStatement += returnCol
		} else {
			returningStatement += fmt.Sprintf(", %s", returnCol)
		}
		var hasCol bool
		for _, sqlCol := range def.dbColsInfo {
			if returnCol == "*" {
				return fmt.Errorf("table %s Returns[%d] %s contains more than one \"*\" values", def.TableName, returnIdx, returnCol)
			}
			if sqlCol.colName == returnCol {
				hasCol = true
				break
			} else {
				continue
			}
		}
		if !hasCol {
			return fmt.Errorf("table %s Returns[%d] %s does not exists", def.TableName, returnIdx, returnCol)
		}
	}
	def.returningStatementPart = returningStatement
	return nil
}
