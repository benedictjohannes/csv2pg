package csv2pg

import (
	"io"
	"log"
	"os"
)

type logWriter interface {
	Print(v ...interface{})
	SetOutput(io.Writer)
}

var mappingLogLevel map[logLevel]int = map[logLevel]int{
	LogDebug:             1,
	LogInfo:              2,
	LogAllTransaction:    3,
	LogFailedTransaction: 4,
	LogSummary:           5,
	LogNone:              6,
}

func (instance *CSV2PG) log(level logLevel, payload string) {
	if mappingLogLevel[level] >= mappingLogLevel[instance.LogLevel] {
		instance.logger.Print(payload)
	}
}
func (instance *aux) log(level logLevel, payload string) {
	if mappingLogLevel[level] >= mappingLogLevel[instance.LogLevel] {
		instance.logger.Print(payload)
	}
}

func (instance *CSV2PG) connectLogger() {
	if instance.logger != nil {
		return
	}
	if instance.LogOutput == nil {
		instance.LogOutput = os.Stdout
	}
	if mappingLogLevel[instance.LogLevel] == 0 {
		instance.LogLevel = LogSummary
	}
	instance.logger = log.New(instance.LogOutput, "", log.LstdFlags)
}

// SetLogger sets the instance's logger output to your own supplied *log.Logger
func (instance *CSV2PG) SetLogger(logger *log.Logger) {
	instance.logger = logger
}
