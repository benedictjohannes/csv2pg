package csv2pg

import (
	"crypto/rand"
	"math"
	"text/template"
	"time"

	"github.com/eknkc/basex"
)

var stdFuncMap = template.FuncMap{
	"currentTimestamp": func(tsFormat string) string {
		if tsFormat == "" {
			tsFormat = "060102150405"
		}
		return time.Now().Format(tsFormat)
	},
	"randomChars": func(digits int, charSpace string) string {
		codeCharsLength := len(charSpace)
		totalCombination := math.Pow(float64(digits), float64(codeCharsLength))
		codeBytes := int(math.Ceil(math.Log(totalCombination) / math.Log(float64(256))))
		randomBytes := make([]byte, codeBytes)
		rand.Read(randomBytes)
		encoder, err := basex.NewEncoding(charSpace)
		if err != nil {
			return ""
		}
		return encoder.Encode(randomBytes)
	},
}
