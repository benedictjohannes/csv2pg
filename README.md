[![Go Reference](https://pkg.go.dev/badge/gitlab.com/benedictjohannes/csv2pg.svg)](https://pkg.go.dev/gitlab.com/benedictjohannes/csv2pg)

# CSV2PG

A Flexible CSV to PostgreSQL reader both usable as standalone CLI and Go module with these features:

-   Flexible CSV columns: CSV columns does not have to translate directly to SQL column names
-   Value validation: Each column in CSV can be validated using regex value
-   Flexible tables: Inserting data in CSV into multiple tables is possible
-   Dynamic values from previous table transaction: If an insertion/tables (`InsertDef`) has `Returns` specified (translats into `returning` statement), the value can be used in the next insertion.  
    This enables insertion into multiple tables with foreign keys, enabling ingestion of denormalized (`JOIN`ed) CSV values.
-   Dynamic values constructed from CSV: Thanks to Go's `text/template` (with some added functions), CSV columns is supplied as a `map[string][string]` that can be accessed to construct dynamic values.  
    This enables features such as columns concatenation, prefixing, suffixing and many more.
-   Unique row identification and update: If column(s) with flag `IsIdentifier` is present, an SQL `select` would be performed to see whether the row has existed.  
    If it exists, if any columns with flag `ShouldUpdate` is present, an `update` with the _identifier_ columns would be performed instead.
-   Logging with multiple level of details.

## Another CSV-to-database software?

Reading CSV into database is a widely (and wildly) available feature offerred in many programming languages and even GUI RDMS frontends.  
However, this aims to be a flexible system to read and process CSV into various SQL structures to enable quick development automated data exchange.

This repository is developed to be a CLI application (under development), where a simple configuration file is enough to process CSV data without `real` software devolopment.

# Usage

## As a Go Module

This is still in heavy development, but usable:

-   Create a new `CSV2PG` instance (designed to be flexible enough to directly be scanned into by YAML or JSON configuration)
-   Significant (non-default) values in each `CSV2PG` struct should be filled
-   The `(*CSV2PG).InitializeAndValidate` function shoud be called, which checks for any misconfigurations of the instance
-   The `(*CSV2PG).ProcessCSV` function can be called to process CSV values supplid (as `io.Reader`)

The type and function documentation sholud provide enough clarity on how to use this in your own application.

## As CLI

You can use this as a generic CLI package, which is developed in [internal > csv2pgcli](internal/csv2pgcli) folder. The CLI version is developed to read CSV and a YAML configuration file and perform the insertion into database. The documentation on CLI usage (how to construct the YAML config etc) is in that folder's [README](internal/csv2pgcli/README.md).
