[![Go Reference](https://pkg.go.dev/badge/gitlab.com/benedictjohannes/csv2pg/internal/csv2pgcli.svg)](https://pkg.go.dev/gitlab.com/benedictjohannes/csv2pg/internal/csv2pgcli)

# CSV2PG Overview

The CLI is built to read `csv` file and process (insert/update) its content into a PGSQL database with _configuration_ supplied via a `yaml` file.

# CLI switches

Below are CLI switches:

-   `-csv`: the path of CSV file (absolute or relative to the `cwd`)
-   `-config`: the path to a yaml file configuring the configuration. Defaults to `config.yaml` (in `cwd`).
-   `-logfile`: path for output of log file. If both this and `-logprefix` is not supplied, logs will be written to `stdout`
-   `-logprefix`: prefix of the log file that will be formatted as suppliedPrefix-{{timestamp}}.log
-   `-logtsfmt`: timestamp that will be used if `-logprefix` is supplied. Defaults to `060102.150405` _(YYMMDD-HHmmss)_
-   `-db`: database connection string. Uses Go's format: `host=%s port=%d user=%s password=%s dbname=%s sslmode=disable` This is not mandatory, where the database connection can be configured via the configuration file

# Configuration file

## Database connection

If database connection is not to be supplied via the CLI `-db` switch, it can be configured via top level keys in the config file as follows:

```yaml
dbhost: 127.0.0.1
dbport: 5432
dbuser: username
dbpass: password
dbname: database
```

## CSV2PG configuration

These are the top level keys to be configured:

-   `loglevel` (string): valid values from most verbose: `debug`, `info`, `allrows`, `failedrows`, `summary`, `none`
-   `stoponcsvrowerror` (boolean): when set to true, will stop when any CSV row processing error occurs
-   `transactionlevel` (string): valid values: `none`, `row` (per CSV rows), `global` (all insertion in one transaction)
-   `csvcommachar` (string): should be a single character controlling the CSV column separator. Must _NOT_ be the newline character.
-   `csvcommentchar` (string): should be a single character, occurence of which in the beginning of line would case the line to be ignored. Must _NOT_ be the newline character.
-   `csvlazyquotes` (boolean): enables some (not all) CSV rows to have quotes (`"`) for the same column.
-   `csvtrimleadingspace` (boolean): trims leading space from CSV (even if separator is set to be space)
-   `csvtrailingcomma` (boolean): enables trailing comma for each CSV row
-   `inserts`: an array of table insertion (`InsertDef`) configuration, see the next section

## `InsertDef` configuration

An `InsertDef` is a single definition of an insertion/update into a single table. Meaning, if the CSV is inserted into just one table, only one single `InsertDef` would be necessary.

Multiple `InsertDef` is supported (hence an instance has an _array_ of `InsertDef`s), which is useful when one single CSV row would be inserting/updating more than one table from the database.

These are the parameters of an `InsertDef`:

-   `tablename` (mandatory): name of the cable in the SQL database
-   `name` (optional): name of an `InsertDef` used when referencing values from previous `InsertDef` returns (like returned auto-generated SQL columns)
-   `returns` (array of string - optional): columns to be cached to be used in next `InsertDef`. If you want all rows, you can set th string `"&"` as the first array members.
-   `shouldupdate` (boolean): columns can have it flagged as `isidentifier` and `shouldupdate`.
-   `columns`: an array of Column definition `ColDef`s, see next section.

## `ColDef` configuration

A `ColDef` lets the inserter know about a column.

This program aims to be able to get values for an `InsertDef`'s SQL column (`ColDef`) via one of several sources:

-   CSV column value, in which case `csvcolname` should be set
-   Dynamic value, for which `csvdynamictext` and `csvdynamictextcols` should be set.  
    Dynamic value utilizes Go's excellent `text/template` package to generate values for the `ColDef`.  
    The template to be executed would be set in `csvdynamictext`, while `csvdynamictextcols` list the CSV column names that the `csvdynamictext` template would reference. Example: if CSV contains `first_name` and `last_name`, we can generate a value for `full_name` column using `{{.first_name}} {{last_name}}`.
    In addition to CSV columns, current timestamp (`currentTimestamp(tsFormat)`) and random characters (`randomChars(nChars, charSpace)`) can be generated.  
    It's best explained via example: `{{- currentTimestamp "060102150405" -}}-{{- (randomChars 6 "123456") -}}` would result in timestamp and x random letters YYMMDDHHmmss-xxxxxx (where x is one of the `123456` specified previously).
-   SQL generated value, such as `now()` or `uuid_v4()`, which is set in `generatorsql` key. This source cannot be `isidentifier`.
-   Previous `InsertDef` result (`Returns`), for instance, auto generated ID from a table, which should be inserted into the next table along with other columns from CSV. For this, the `FromPath` key should be set to an array of exactly 2 string: (1) Previous `InsertDef.name` and (2) `InsertDef.sqlcolname`.

Each column value (CSV column or dynamic text data source) can be validated using `validationregex` key: any value _not_ satisfying the `validationregex` would cause error/failed insertion.

Each column _must_ provide `sqlcolname` and `sqlcoltype`, both of which would be vaidated on the program startup. The supported `sqlcoltype` values:

-   `bool` for which `booleanregex` key should be set. Matching regex would be treated as true value.
-   `text` (where `varchar(n)` columns should be set to this)
-   `timestamptz` for which `timestampformat` should be set to Go's time format.
-   `timestamp` for which `timestampformat` should be set to Go's time format.
-   `date` for which `timestampformat` should be set to Go's time format.
-   `float`
-   `int`
-   `uuid`

`ColDef` also has these flags available pertaining to insert/update flow:

-   `isidentifier`: a true value means that a column is part of columns uniquely identifying rows/records of a table
-   `shouldupdate`: a true value means that the column's value should be updated, where the row is identified by `isidentifier` columns

In short: The full list of keys for `ColDef`:

-   `csvcolname` (data source 1)
-   `csvdynamictext` (data source 2)
-   `csvdynamictextcols` (data source 2)
-   `generatorsql` (data source 3)
-   `frompath` (data source 4)
-   `timestampformat` (only for ``timestamptz`, `timestamp`, and `date` column type)
-   `booleanregex` (only for `bool` column type)
-   `validationregex` (only for data source 1 and 2)
-   `sqlcolname`
-   `sqlcoltype`
-   `isidentifier` (exclusive with `shouldupdate`)
-   `shouldupdate` (exclusive with `isidentifier`)

## Note on `InsertDef` _"update or insert"_ (aka upsert) feature

The true `insert ... on conflict ... do update ...``` from PGSQL does amazing things, but it necessitates a unique constraint.  
This program takes a different approach.

It relies on these flags:

-   `shouldupdate` flag on `InsertDef` (_table_)
-   `isidentifier` flag on `ColDef` (_column_)
-   `shouldupdate` flag on `ColDef` (_column_)

Here are the logic:

-   If a _table_ have one or more `isidentifier` columns, a `select` statement would be performed before the values are inserted into the database (`isidentifier` becomes the where conditions). If no `isidentifier` columns are present, the insertion will be executed immediately without the `select` statement.
-   If the table (`InsertDef`) has its `shouldupdate` set to false (the default), if an SQL record satisfying the `isidentifier` columns exists, it would cause the current CSV row's table insertion to fail.
-   If the table (`InsertDef`) has its `shouldupdate` set to true, the `InsertDef` will not fail due to preexisting SQL record. If _any_ columns has `shouldupdate` set to true, an `update` statement will be performed, updating `shouldupdate` columns with the `isidentifier` columns as where condition. Thus, if a table has `shouldupdate` set to true and has `shouldupdate` columns, `isidentifier` columns should uniquely identify _single rows of update_, otherwise, more than one rows might be updated.

# Example

Given these SQL tables:
```sql
create extension if not exists uuid_ossp;
create table t1 (
    uuidcol uuid primary key default uuid_generate_v4(),
    textcol text,
    timestampcol timestamptz,
    boolcol bool,
    datecol date
);
create table t2 (
    uuidcol uuid,
    intcol int,
    floatcol float,
    randomtextcol text,
    foreign key (uuidcol) references t1(uuidcol)
);
```

and this CSV file
```csv
firstname;lastname;dob;quote;isdummy;RaNdOm_FlOaT;random integer
foo;bar;01-04-2014;It's April Fools Day!;yes;4.1;41
rob;pike;01-01-1956;"Dependency hygiene trumps code reuse";no;1.1;2
ken;thompson;04-02-1943;"When in doubt, use brute force";no;3.3;4
should;fail;04-01-2014;It's January 4th!;nox;1.4;14
# as the above row would fail, this below row won't be processed if stoponcsvrowerror set to true
# and if transaction level set to global, the whole inserted values would be rolled back
bar;foo;01-04-2041;Reverse the entry name;no;1.4;14
```

This would be a valid configuration:
```yaml
# DB connection parameters can either be defined here or by passing to -db the formatted connection parameters
dbhost: 127.0.0.1
dbport: 5432
dbuser: test
dbpass: test
dbname: test
# valid values from most verbose: 'debug', 'info', 'allrows', 'failedrows', 'summary', 'none'
loglevel: debug
# when set to true, will stop when any CSV row processing error occurs
stoponcsvrowerror: true
# valid values: 'none', 'row' (per CSV rows), 'global' (all insertion in one transaction)
transactionlevel: global
# csv parameters are passed to encoding/csv
csvcommachar: ;
csvcommentchar: '#'
csvlazyquotes: true
csvtrimleadingspace: true
csvtrailingcomma: false
inserts:
    - tablename: t1
      name: firstinsertion
      shouldupdate: true
      returns:
          - '*' # '*' cannot be second entry
      columns:
          - sqlcolname: textcol
            csvdynamictext: '{{- .firstname -}}-{{- .lastname -}}'
            sqlcoltype: text
            shouldupdate: true
          - sqlcolname: boolcol
            csvcolname: isdummy
            sqlcoltype: bool
            shouldupdate: true
            booleanregex: 'yes'
            validationregex: '^yes$|^no$'
          - sqlcolname: timestampcol
            sqlcoltype: timestamptz
            generatorsql: now()
            shouldupdate: true
          - sqlcolname: datecol
            sqlcoltype: date
            csvcolname: dob
            timestampformat: '02-01-2006'
            isidentifier: true
    - tablename: t2
      shouldupdate: true
      columns:
          - sqlcolname: 'floatcol'
            sqlcoltype: float
            csvcolname: 'RaNdOm_FlOaT'
            shouldupdate: true
          - sqlcolname: 'intcol'
            sqlcoltype: int
            csvcolname: 'random integer'
          - sqlcolname: 'randomtextcol'
            sqlcoltype: text
            csvdynamictext: '{{ (randomChars 6 "abcdef") -}}-{{- currentTimestamp "060102150405" }}'
          - sqlcolname: uuidcol
            sqlcoltype: uuid
            isidentifier: true
            frompath:
                - firstinsertion
                - uuidcol
```

The example, though brief, demostrates the use cases for CSV insertion that usually has required developers to code programs to ingest the CSV.