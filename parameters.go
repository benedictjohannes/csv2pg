package csv2pg

import (
	"database/sql"
	"io"
	"regexp"
	"text/template"
)

type columnType string

const (
	// BoolCol is a text column represented as Go's string type
	BoolCol columnType = "bool"
	// StringCol is a text column represented as Go's string type
	StringCol columnType = "text"
	// TimestampTzCol is a timestamptz column to be represented as Go's time type
	TimestampTzCol columnType = "timestamptz"
	// TimestampCol is a timestamp column to be represented as Go's time type
	TimestampCol columnType = "timestamp"
	// DateCol is a date column to be represented as Go's time type
	DateCol columnType = "date"
	// FloatCol is a float column represented as Go's float type
	FloatCol columnType = "float"
	// IntCol is a int column represented as Go's int type
	IntCol columnType = "int"
	// UUIDCol is a timestamptz column represented as Go's google/uuid type
	UUIDCol columnType = "uuid"
)

// ColDef is a single column definition that
// maps the name of CSV column to the destination table column.
//
// Note that (1) CSVColName, CSVDynamicText+CSVDynamicTextCols, GeneratorSQL and FromPath
// should not be set together, (2) IsIdentifier and ShouldUpdate should not both be true and
// (3) invalid SQLColType, SQLColName, CSVColName would
// cause *CSV2PG.InitializeAndValidate to fail
type ColDef struct {
	// CSVColName is the CSV header column name to be read
	CSVColName string
	// CSVToText is to feed arbitrary string into the column inserted
	// Uses the Go text/template syntax
	// The CSVToTextCols should be set for validation
	CSVDynamicText     string
	CSVDynamicTextCols []string
	// ValidationRegex would be checked for CSVDynamicText or CSVCol is defined to be
	// source of data for the column
	ValidationRegex string
	// ColName is the column name in the database table
	SQLColName string
	// IsIdentifier when any column is an identifier, a select statement would be performed
	// before any insertion to check WHERE conditions matching the identifier already exists
	// Only one row is scanned, thus, all unique rows should be identified with IsIdentifier columns
	// If no update is performed, the returned select * would be used for next FromPath InsertDefs
	IsIdentifier bool
	// ShouldUpdate controls whether the column should be updated if the InsertDef
	// mandates updating the SQL row for existing IsIdentifier column
	// If no column is marked ShouldUpdate, update would not be executed
	// when row satisfying IsIdentifer columns exists
	// and the select statement result would be accessible in Returns FromPath
	ShouldUpdate bool
	// Timestamp format using Go's time reference timestamp, i.e. "2006-01-02 15:04"
	// Should be set if the column type is timestamp
	TimestampFormat string
	// Should be filled with regex for which match with the CSV value would produce true
	BooleanRegex string
	// ColType provides the information on how to typecast the CSV column
	// into Go types that is to be transferred into PostgreSQL
	SQLColType columnType
	// A SQL expression that would be used in generating a column.
	// i.e. now() or uuid_generate_v4()
	GeneratorSQL string
	// Accesses a stored value from previous InsertDef
	// First array member should be the previous InsertDef.Name
	// Second array member should be the name of column in InsertDef.Returns
	FromPath            []string
	colSource           string
	dynamicTextTemplate *template.Template
	validationRegexp    *regexp.Regexp
	booleanRegexp       *regexp.Regexp
	bindArrIdx          int
}

// InsertDef is a single insertion to be performed on a single CSV row
//
// Insertion into a single table should be defined into a single insertion
type InsertDef struct {
	// Used to identify the InsertDef if "returning" set in Returns is used in next InsertDef
	Name      string
	TableName string
	Columns   []*ColDef
	// If not set, the SQL would not be appended with "returning" statements
	Returns []string
	// ShouldUpdate controls whether if a row with matching WHERE conditions
	// for all IsIdentifier columns, an UPDATE statement would be performed instead.
	// If any column is marked IsIdentifier, checking for existing row would still be performed if
	// any column is marked ShouldUpdate
	// But update won't be executed and the row insertion will error out
	ShouldUpdate           bool
	returnAllArr           []string
	dbColsInfo             []dbColDef
	insertStatement        string
	insertBindsLength      int
	haveIdentifiers        bool
	selectStatement        string
	selectBindsLength      int
	haveUpdatables         bool
	updateStatement        string
	updateBindsLength      int
	returningStatementPart string
}

type dbColDef struct{ colName, dataType string }

type logLevel string

const (
	// LogDebug will log all insertion activites with maximum detail
	LogDebug logLevel = "debug"
	// LogInfo will log all activites for all insertion, updates etc
	LogInfo logLevel = "info"
	// LogAllTransaction will log SUCCESS|FAIL <raw csv value>
	LogAllTransaction logLevel = "allrows"
	// LogFailedTransaction will only log failed insert: FAIL <raw csv value>
	LogFailedTransaction logLevel = "failedrows"
	// LogSummary will log only summary of start and completion time and number of successful inserts
	LogSummary logLevel = "summary"
	// LogNone will log nothing
	LogNone logLevel = "none"
)

type transactionLevel string

const (
	// TransactionLevelNone is for no transactions
	TransactionLevelNone transactionLevel = "none"
	// TransactionLevelRow is for one single transaction per CSV row
	TransactionLevelRow transactionLevel = "row"
	// TransactionLevelGlobal is for one single transaction for all contents of CSV file
	TransactionLevelGlobal transactionLevel = "global"
)

// CSV2PG defines how the insertion is to be performed
type CSV2PG struct {
	// valid values from most verbose: 'debug', 'info', 'allrows', 'failedrows', 'summary', 'none'
	LogLevel logLevel
	// destination of log outputs. If not set, it would be set to os.Stdout
	LogOutput io.Writer
	// when set to true, will stop when any CSV row processing error occurs
	StopOnCSVRowError bool
	// valid values: 'none', 'row' (per CSV rows), 'global' (all insertion in one transaction)
	TransactionLevel    transactionLevel
	Inserts             []*InsertDef
	CSVCommaChar        string
	CSVCommentChar      string
	CSVLazyQuotes       bool
	CSVTrimLeadingSpace bool
	CSVTrailingComma    bool
	// 2 standard funcmap is provided by default: currentTimestamp randomChars
	// If you provide funcmap, it would be merged with the standards
	// signatures:
	// -  currentTimestamp(timestampFormat string) <-- Go's standard timestamp spec
	// -  randomChars(digits int, charSpace string) <-- charSpace should not contain repeating string
	FuncMaps    template.FuncMap
	logger      logWriter
	db          *sql.DB
	tx          *sql.Tx
	csvHeaders  []string
	initialized bool
}

type aux struct {
	LogLevel          logLevel
	LogOutput         io.Writer
	TransactionLevel  transactionLevel
	StopOnInsertError bool
	FuncMaps          template.FuncMap
	logger            logWriter
	db                *sql.DB
	tx                *sql.Tx
}

func (instance *CSV2PG) getAuxiliaries() (aux aux) {
	aux.LogLevel = instance.LogLevel
	aux.LogOutput = instance.LogOutput
	aux.TransactionLevel = instance.TransactionLevel
	aux.StopOnInsertError = instance.StopOnCSVRowError
	aux.FuncMaps = instance.FuncMaps
	aux.logger = instance.logger
	aux.db = instance.db
	aux.tx = instance.tx
	return aux
}
