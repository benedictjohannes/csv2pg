package main

import (
	"database/sql"
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	"gitlab.com/benedictjohannes/csv2pg"

	_ "github.com/lib/pq"
	"gopkg.in/yaml.v2"
)

type dbConfig struct {
	DbHost string
	DbPort int
	DbUser string
	DbPass string
	DbName string
}

func readConfigToInstance(cfgPath string) (insertInstance csv2pg.CSV2PG, err error) {
	file, err := os.Open(cfgPath)
	if err != nil {
		if os.IsNotExist(err) {
			log.Fatal("config.yaml does not exist")
		}
		log.Fatalln("Failed to read configuration file")
	}
	yamlDecoder := yaml.NewDecoder(file)
	err = yamlDecoder.Decode(&insertInstance)
	if err != nil {
		return insertInstance, fmt.Errorf("failed to decode configuration file: %s", err)
	}
	return insertInstance, nil
}

func readConfigToDbConfig(cfgPath string) (dbConfig dbConfig, err error) {
	file, err := os.Open(cfgPath)
	if err != nil {
		if os.IsNotExist(err) {
			log.Fatal("config.yaml does not exist")
		}
		log.Fatalln("Failed to read configuration file")
	}
	yamlDecoder := yaml.NewDecoder(file)
	err = yamlDecoder.Decode(&dbConfig)
	if err != nil {
		return dbConfig, fmt.Errorf("failed to decode configuration file: %s", err)
	}
	return dbConfig, nil
}

func main() {
	logFilePtr := flag.String("logfile", "", "Log file write logs into")
	logFilePrefixPtr := flag.String("logprefix", "", "Log file prefix to write logs into. Will be written as `suppliedPrefix-{{logtsfmt}}.log`")
	logTsPtr := flag.String("logtsfmt", "", "Timestamp for log file. Defaults to YYMMDD-HHmmss")
	csvFilePtr := flag.String("csv", "", "CSV file to be processed")
	configPtr := flag.String("config", "config.yaml", "Defines the value of config.yaml. Defaults to config.yaml")
	dbConnPtr := flag.String("db", "", "Database connection string in format: 'host=%s port=%d user=%s password=%s dbname=%s sslmode=disable'")
	flag.Parse()
	instance, err := readConfigToInstance(*configPtr)
	if *logFilePtr != "" {
		logDest, err := os.OpenFile(*logFilePtr, os.O_RDWR|os.O_CREATE, 0644)
		if err != nil {
			log.Fatalln(err)
		}
		instance.LogOutput = logDest
	}
	if *logFilePrefixPtr != "" {
		tsFmt := "060102.150405"
		if *logTsPtr != "" {
			tsFmt = *logTsPtr
		}
		logFileName := fmt.Sprintf("%s-%s.log", *logFilePrefixPtr, time.Now().Format(tsFmt))
		logDest, err := os.OpenFile(logFileName, os.O_RDWR|os.O_CREATE, 0644)
		if err != nil {
			log.Fatalln(err)
		}
		instance.LogOutput = logDest
	}
	if err != nil {
		log.Fatalln(err)
	}
	if *dbConnPtr == "" {
		dbConfig, err := readConfigToDbConfig(*configPtr)
		if err != nil {
			log.Fatalln(err)
		}
		pgConnStr := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
			dbConfig.DbHost,
			dbConfig.DbPort,
			dbConfig.DbUser,
			dbConfig.DbPass,
			dbConfig.DbName,
		)
		dbConnPtr = &pgConnStr
	}
	pgConn, err := sql.Open("postgres", *dbConnPtr)
	if err != nil {
		log.Fatalln("Failed to connect to database")
	}
	csvFile, err := os.Open(*csvFilePtr)
	if err != nil {
		log.Fatalln("fail to open csv")
	}
	instance.ProcessCSV(pgConn, csvFile)
}
